extends Node2D

@export var speed = 3

var tile_size = 64
var active_collision_count = 0
var vel := Vector2.ZERO
var active := false
var drowning := false
var tween: Tween
var logger: Logger

@onready var splat_anim = preload("res://Art/Splat.tscn")
@onready var splash_anim = preload("res://Art/Splash.tscn")
@onready var bonk_anim = preload("res://Art/Bonk.tscn")

signal died
signal reached_home
signal hop


func _ready():
	await get_tree().create_timer(0.2).timeout
	active = true
	$AnimatedSprite2D.speed_scale = speed
	logger = Print.create_logger("Player", Print.VERBOSE, Print.VERBOSE)


func _process(delta):
	if active:
		if tween == null or not tween.is_running():
			if drowning:
				drown()
			process_inputs()
		position += vel * 64 * delta * Global.game_speed


func process_inputs():
	if Input.is_action_pressed("Up"):
		start_move(Vector2.UP)
		$AnimatedSprite2D.rotation_degrees = 0
	if Input.is_action_pressed("Down"):
		start_move(Vector2.DOWN)
		$AnimatedSprite2D.rotation_degrees = 180
	if Input.is_action_pressed("Left"):
		start_move(Vector2.LEFT)
		$AnimatedSprite2D.rotation_degrees = 270
	if Input.is_action_pressed("Right"):
		start_move(Vector2.RIGHT)
		$AnimatedSprite2D.rotation_degrees = 90


func start_move(dir):
	tween = get_tree().create_tween()
	var end_position = position + dir * tile_size  # Calculate the end position outside the method call
	tween.tween_property(self, "position", end_position, 1.0 / speed).set_trans(Tween.TRANS_SINE).set_ease(Tween.EASE_IN_OUT)
	tween.play()
	$AnimatedSprite2D.frame = 0
	$AnimatedSprite2D.play()
	
	if dir == Vector2.UP:
		emit_signal("hop")


func splat():
	logger.info("Splatted")
	active = false
	queue_free()
	emit_signal("died")


func drown():
	logger.info( "Drowned")
	queue_free()
	var splash = splash_anim.instantiate()
	splash.global_position = global_position
	get_parent().add_child(splash)
	emit_signal("died")


func win(pos: Vector2):
	logger.info("Frog reached home!")
	active = false
	emit_signal("reached_home")
	tween.kill()
	position = pos
	$AnimatedSprite2D.rotation_degrees = 180
	$AudioStreamPlayer.play()


func _on_LogCollider_body_entered(body):
	active_collision_count += 1
	if body is Obstacle:
		vel = body.vel
	else:
		vel = Vector2.ZERO


func _on_LogCollider_body_exited(_body):
	active_collision_count -= 1
	if active_collision_count == 0:
		vel = Vector2.ZERO
		if active and position.y < 448:
			drowning = true


func _on_SplatCollider_body_entered(_body):
	if active:
		var splat_a = splat_anim.instantiate()
		splat_a.global_position = global_position
		get_parent().add_child(splat_a)
		splat()


func _on_Home_Collider_body_entered(body):
	win(body.position)
	body.queue_free()


func _on_Barrier_Collider_body_entered(_body):
	if active:
		var bonk = bonk_anim.instantiate()
		bonk.global_position = global_position
		get_parent().add_child(bonk)
		splat()

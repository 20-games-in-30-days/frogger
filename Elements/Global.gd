extends Node2D

@export var game_speed := 0.5
@export var speed_factor := 0.3
@export var game_over := false

func _ready():
	Console.add_command("game_speed", self, "set_game_speed").\
	add_argument("speed", TYPE_INT).register()


func set_game_speed(speed):
	game_speed = speed

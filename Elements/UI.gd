extends CanvasLayer

signal time_out
var score := 0
var time_left := 30.0
var level_active := true


func new_game(lives: int):
	time_left = 30.0
	draw_lives(lives)
	$GameText.visible = false

func reset():
	score = 0
	Global.game_speed = 0.5


func new_life(lives: int):
	draw_lives(lives)
	time_left = 30.0
	level_active = true


func on_home_reached():
	add_score(50 + int(time_left * 2))
	level_active = false


func level_won():
	add_score(1000)
	$GameText.text = "Level Cleared!"
	$GameText.visible = true


func add_score(value: int):
	score += value
	$Score.text = str(score)


func draw_lives(lives: int):
	$Life0.visible = lives >= 1
	$Life1.visible = lives >= 2
	$Life2.visible = lives >= 3
	$Life3.visible = lives >= 4
	$Life4.visible = lives >= 5


func game_over():
	$GameText.text = "Game Over!"
	$GameText.visible = true


func _process(delta):
	time_left -= delta
	$Time.value = time_left
	if time_left <= 0 and level_active:
		emit_signal("time_out")
		level_active = false

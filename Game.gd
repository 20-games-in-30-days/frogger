extends CanvasLayer

var player_prefab = preload("res://Elements/Player.tscn")
var lives_left := 3
var homes_left := 5
var player: Node2D
var level: Node2D
var next_level_id := 0
@export var levels := [] # (Array, Resource)


func _ready():
	reset()
	$Background/RiverAnim.play("Flow")
	
	# Attach some console commands for debugging.
	Console.add_command("frog_reached_home", self, "_on_Player_reached_home").register()
	Console.add_command("time_out", self, "_on_time_out").register()
	Console.add_command("next_level", self, "reset").register()


func reset():
	lives_left = 5
	homes_left = 5
	$UI.new_game(lives_left)
	clear_level()
	await get_tree().process_frame
	create_player()
	
	# Create the level
	level = levels[next_level_id].instantiate()
	add_child(level)
	
	# Load the next level
	next_level_id += 1
	if next_level_id >= levels.size():
		next_level_id = 0
	
	$Background/RiverAnim.speed_scale = Global.game_speed


func create_player():
	player = player_prefab.instantiate()
	var _d # Discard
	_d = player.connect("died", Callable(self, "_on_Player_died"))
	_d = player.connect("reached_home", Callable(self, "_on_Player_reached_home"))
	_d = player.connect("hop", Callable(self, "_on_Player_hop"))
	call_deferred("add_child", player)
	player.position = Vector2(800, 864)


func player_died():
	player = null
	if lives_left > 0:
		lives_left -= 1
		await get_tree().create_timer(1.0).timeout
		create_player()
		$UI.new_life(lives_left)
	else:
		$UI.game_over()
		next_level_id = 0
		await get_tree().create_timer(1.0).timeout
		Global.game_over = true


func _input(event):
	if Global.game_over and (\
		event.is_action_pressed("Up")\
		or event.is_action_pressed("Left")\
		or event.is_action_pressed("Right")\
		or event.is_action_pressed("Down")\
		or event.is_action_pressed("Select")):
		Global.game_over = false
		$UI.reset()
		reset()


func _on_Player_hop():
	$UI.add_score(10)


func _on_Player_died():
	player_died()


func clear_level():
	if level != null and is_instance_valid(level):
		level.queue_free()
	# Clear all frogs in homes, as well as the player.
	for frog in get_tree().get_nodes_in_group("Frog"):
		if is_instance_valid(frog):
			frog.queue_free()


func _on_Player_reached_home():
	$UI.on_home_reached()
	homes_left -= 1
	if homes_left > 0:
		create_player()
		$UI.new_life(lives_left)
	else:
		$UI.level_won()
		await get_tree().create_timer(5.0).timeout
		Global.game_speed += Global.speed_factor
		reset()


func _on_time_out():
	if player != null and is_instance_valid(player):
		player.queue_free()
		player_died()

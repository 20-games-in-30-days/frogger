class_name Obstacle
extends CharacterBody2D

@export var vel: Vector2


func _physics_process(_delta):
	set_velocity(vel * 64 * Global.game_speed)
	move_and_slide()
	var _v = vel
	
	if vel.x > 0 and global_position.x > 1920:
		if Global.game_over:
			queue_free()
		else:
			global_position.x -= 2240
	elif vel.x < 0 and global_position.x < -320:
		if Global.game_over:
			queue_free()
		else:
			global_position.x += 2240
